Source: ivar
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libhts-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/ivar
Vcs-Git: https://salsa.debian.org/med-team/ivar.git
Homepage: https://github.com/andersen-lab/ivar
Rules-Requires-Root: no

Package: ivar
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: samtools
Description: functions broadly useful for viral amplicon-based sequencing
 iVar is a computational package that contains functions broadly useful
 for viral amplicon-based sequencing. Additional tools for metagenomic
 sequencing are actively being incorporated into iVar. While each of
 these functions can be accomplished using existing tools, iVar contains
 an intersection of functionality from multiple tools that are required
 to call iSNVs and consensus sequences from viral sequencing data across
 multiple replicates. iVar provided the following functions:
 .
  1. trimming of primers and low-quality bases,
  2. consensus calling,
  3. variant calling - both iSNVs and insertions/deletions, and
  4. identifying mismatches to primer sequences and excluding the
     corresponding reads from alignment files.

Package: ivar-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         node-jquery
Multi-Arch: foreign
Description: functions broadly useful for viral amplicon-based sequencing (documentation)
 iVar is a computational package that contains functions broadly useful
 for viral amplicon-based sequencing. Additional tools for metagenomic
 sequencing are actively being incorporated into iVar. While each of
 these functions can be accomplished using existing tools, iVar contains
 an intersection of functionality from multiple tools that are required
 to call iSNVs and consensus sequences from viral sequencing data across
 multiple replicates. iVar provided the following functions:
 .
  1. trimming of primers and low-quality bases,
  2. consensus calling,
  3. variant calling - both iSNVs and insertions/deletions, and
  4. identifying mismatches to primer sequences and excluding the
     corresponding reads from alignment files.
 .
 This package contains the html documentation for ivar.
